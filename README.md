## Manual Steps
```sh
# This was done to disable tls and should be done on the initial argocd install, instead.
# Get a bash script that uses the argocd cli to create and configure the first app to point to atlas
# Get some other repos for infra related things like namespaces, network policies
kubectl apply -n argocd -f ./infra/argo-service.yaml
```

## Quick Links
[ArgoCD](https://argocd-127-0-0-1.nip.io/)
[Grafana](https://grafana-127-0-0-1.nip.io/)
[Prometheus](https://prometheus-127-0-0-1.nip.io/)
[Traefik Dashboard](https://argocd-127-0-0-1.nip.io/)

# Grafana login
```sh
kubectl get secret --namespace prom-stack kube-prometheus-stack-grafana -o jsonpath="{.data.admin-password}" | base64 --decode ; echo
kubectl get secret --namespace prom-stack kube-prometheus-stack-grafana -o jsonpath="{.data.admin-user}" | base64 --decode ; echo
```